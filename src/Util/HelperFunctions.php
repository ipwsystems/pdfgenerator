<?php

namespace IpwSystems\PdfGenerator\Util;

class HelperFunctions
{
    private function __construct()
    {
        // Intentionally left blank - don't instantiate this class.
    }

    /**
     * Create a new unique file.
     *
     * @param string|null $dir The directory where the file should be located. If null, use default tmp dir.
     * @param string|null $prefix An optional prefix for the file.
     * @return array An array with two elements, the full path to the file, and the unique id of the file.
     * @throws \Exception
     */
    public static function createUniqueFile($dir, $prefix = null)
    {
        if (!file_exists($dir)) {
            throw new \Exception("Directory '{$dir}' couldn't be found.");
        }

        do {
            $name = uniqid($prefix);
            $file = rtrim($dir, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $name;
        } while (file_exists($file));

        $created = touch($file);

        if (!$created) {
            throw new \Exception("File '{$file}' couldn't be created.");
        }

        return [$file, $name];
    }

    /**
     * Flatten a multi-dimensional array to a one-dimensional array.
     *
     * @param array|mixed $array
     * @return array
     */
    public static function array_flatten($array = null)
    {
        $result = [];

        if (!is_array($array)) {
            $array = func_get_args();
        }

        foreach ($array as $key => $value) {
            $result = array_merge(
                $result,
                is_array($value) ? self::array_flatten($value) : [$key => $value]
            );
        }

        return $result;
    }
}
