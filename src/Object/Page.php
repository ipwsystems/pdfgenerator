<?php

namespace IpwSystems\PdfGenerator\Object;

class Page extends Options
{
    const TYPE = 'page';

    /**
     * --allow <path>                  Allow the file or files from the specified folder to be loaded (repeatable)
     *
     * @param string $path
     * @return Page
     */
    public function allow($path)
    {
        $this->add('allow', $path, true);
        return $this;
    }

    /**
     * --background                    Do print background (default)
     * @return Page
     */
    public function background()
    {
        $this->add('background');
        return $this;
    }

    /**
     * --no-background                 Do not print background
     * @return Page
     */
    public function noBackground()
    {
        $this->add('no-background');
        return $this;
    }

    /**
     * --bypass-proxy-for <value>      Bypass proxy for host (repeatable)
     * @param $value
     * @return Page
     */
    public function bypassProxyFor($value)
    {
        $this->add('bypass-proxy-for', $value, true);
        return $this;
    }

    /**
     * --cache-dir <path>              Web cache directory
     * @param $path
     * @return Page
     */
    public function cacheDir($path)
    {
        $this->add('cache-dir', $path);
        return $this;
    }

    /**
     * --checkbox-checked-svg <path>   Use this SVG file when rendering checked checkboxes
     * @param $path
     * @return Page
     */
    public function checkboxCheckedSvg($path)
    {
        $this->add('checkbox-checked-svg', $path);
        return $this;
    }

    /**
     * --checkbox-svg <path>           Use this SVG file when rendering unchecked checkboxes
     * @param $path
     * @return Page
     */
    public function checkboxSvg($path)
    {
        $this->add('checkbox-svg', $path);
        return $this;
    }

    /**
     * --cookie <name> <value>         Set an additional cookie (repeatable), value should be url encoded.
     * @param string $name
     * @param string $value
     * @return Page
     */
    public function cookie($name, $value)
    {
        $this->add('cookie', ['name' => $name, 'value' => urlencode($value)], true);
        return $this;
    }

    /**
     * --custom-header <name> <value>  Set an additional HTTP header (repeatable)
     * @param $value
     * @return Page
     */
    public function customHeader($value)
    {
        $this->add('custom-header', $value, true);
        return $this;
    }

    /**
     * --custom-header-propagation     Add HTTP headers specified by --custom-header for each resource request.
     * @return Page
     */
    public function customHeaderPropagation()
    {
        $this->add('custom-header-propagation');
        return $this;
    }

    /**
     * --no-custom-header-propagation  Do not add HTTP headers specified by --custom-header for each resource request.
     * @return Page
     */
    public function noCustomHeaderPropagation()
    {
        $this->add('no-custom-header-propagation');
        return $this;
    }

    /**
     * --debug-javascript              Show javascript debugging output
     * @return Page
     */
    public function debugJavascript()
    {
        $this->add('debug-javascript');
        return $this;
    }

    /**
     * --no-debug-javascript           Do not show javascript debugging output (default)
     * @return Page
     */
    public function noDebugJavascript()
    {
        $this->add('no-debug-javascript');
        return $this;
    }

    /**
     * --default-header                Add a default header, with the name of the page to the left, and the page number to the right, this is short for: --header-left='[webpage]' --header-right='[page]/[toPage]' --top 2cm --header-line
     * @return Page
     */
    public function defaultHeader()
    {
        $this->add('default-header');
        return $this;
    }

    /**
     * --encoding <encoding>           Set the default text encoding, for input
     * @param $encoding
     * @return Page
     */
    public function encoding($encoding)
    {
        $this->add('encoding', $encoding);
        return $this;
    }

    /**
     * --disable-external-links        Do not make links to remote web pages
     * @return Page
     */
    public function disableExternalLinks()
    {
        $this->add('disable-external-links');
        return $this;
    }

    /**
     * --enable-external-links         Make links to remote web pages (default)
     * @return Page
     */
    public function enableExternalLinks()
    {
        $this->add('enable-external-links');
        return $this;
    }

    /**
     * --disable-forms                 Do not turn HTML form fields into pdf form fields (default)
     * @return Page
     */
    public function disableForms()
    {
        $this->add('disable-forms');
        return $this;
    }

    /**
     * --enable-forms                  Turn HTML form fields into pdf form fields
     * @return Page
     */
    public function enableForms()
    {
        $this->add('disable-forms');
        return $this;
    }

    /**
     * --images                        Do load or print images (default)
     * @return Page
     */
    public function images()
    {
        $this->add('images');
        return $this;
    }

    /**
     * --no-images                     Do not load or print images
     * @return Page
     */
    public function noImages()
    {
        $this->add('no-images');
        return $this;
    }

    /**
     * --disable-internal-links        Do not make local links
     * @return Page
     */
    public function disableInternalLinks()
    {
        $this->add('disable-internal-links');
        return $this;
    }

    /**
     * --enable-internal-links         Make local links (default)
     * @return Page
     */
    public function enableInternalLinks()
    {
        $this->add('enable-internal-links');
        return $this;
    }

    /**
     * --disable-javascript            Do not allow web pages to run javascript
     * @return Page
     */
    public function disableJavascript()
    {
        $this->add('disable-javascript');
        return $this;
    }

    /**
     * --enable-javascript             Do allow web pages to run javascript (default)
     * @return Page
     */
    public function enableJavascript()
    {
        $this->add('enable-javascript');
        return $this;
    }

    /**
     * --javascript-delay <msec>       Wait some milliseconds for javascript finish (default 200)
     * @param int $msec Number of milliseconds to wait for javascript to finish.
     * @return Page
     */
    public function javascriptDelay($msec)
    {
        $this->add('javascript-delay', $msec);
        return $this;
    }

    /**
     * --keep-relative-links           Keep relative external links as relative external links
     * @return Page
     */
    public function keepRelativeLinks()
    {
        $this->add('keep-relative-links');
        return $this;
    }

    /**
     * --load-error-handling <handler> Specify how to handle pages that fail to load: abort, ignore or skip (default abort)
     * @param string $handler Either 'abort' (default), 'ignore' or 'skip'
     * @return Page
     */
    public function loadErrorHandling($handler)
    {
        $this->add('load-error-handling', $handler);
        return $this;
    }

    /**
     * --load-media-error-handling <handler> Specify how to handle media files that fail to load: abort, ignore or skip (default ignore)
     * @param string $handler Either 'abort' (default), 'ignore' or 'skip'
     * @return Page
     */
    public function loadMediaErrorHandling($handler)
    {
        $this->add('load-media-error-handling', $handler);
        return $this;
    }

    /**
     * --disable-local-file-access     Do not allowed conversion of a local file to read in other local files, unless explicitly allowed with --allow
     * @return Page
     */
    public function disableLocalFileAccess()
    {
        $this->add('disable-local-file-access');
        return $this;
    }

    /**
     * --enable-local-file-access      Allowed conversion of a local file to read in other local files. (default)
     * @return Page
     */
    public function enableLocalFileAccess()
    {
        $this->add('enable-local-file-access');
        return $this;
    }

    /**
     * --minimum-font-size <int>       Minimum font size
     * @param $int
     * @return Page
     */
    public function minimumFontSize($int)
    {
        $this->add('minimum-font-size', $int);
        return $this;
    }

    /**
     * --exclude-from-outline          Do not include the page in the table of contents and outlines
     * @return Page
     */
    public function excludeFromOutline()
    {
        $this->add('exclude-from-outline');
        return $this;
    }

    /**
     * --include-in-outline            Include the page in the table of contents  and outlines (default)
     * @return Page
     */
    public function includeInOutline()
    {
        $this->add('include-in-outline');
        return $this;
    }

    /**
     * --page-offset <offset>          Set the starting page number (default 0)
     * @param int $offset
     * @return Page
     */
    public function pageOffset($offset)
    {
        $this->add('page-offset', $offset);
        return $this;
    }

    /**
     * --password <password>           HTTP Authentication password
     * @param string $password
     * @return Page
     */
    public function password($password)
    {
        $this->add('password', $password);
        return $this;
    }

    /**
     * --disable-plugins               Disable installed plugins (default)
     * @return Page
     */
    public function disablePlugins()
    {
        $this->add('disable-plugins');
        return $this;
    }

    /**
     * --enable-plugins                Enable installed plugins (plugins will likely not work)
     * @return Page
     */
    public function enablePlugins()
    {
        $this->add('enable-plugins');
        return $this;
    }

    /**
     * --post <name> <value>           Add an additional post field (repeatable)
     * @param $value
     * @return Page
     */
    public function post($value)
    {
        $this->add('post', $value, true);
        return $this;
    }

    /**
     * --post-file <name> <path>       Post an additional file (repeatable)
     * @param $name
     * @param $path
     * @return Page
     */
    public function postFile($name, $path)
    {
        $this->add('post-file', [$name, $path], true);
        return $this;
    }

    /**
     * --print-media-type              Use print media-type instead of screen
     * @return Page
     */
    public function printMediaType()
    {
        $this->add('print-media-type');
        return $this;
    }

    /**
     * --no-print-media-type           Do not use print media-type instead of screen (default)
     * @return Page
     */
    public function noPrintMediaType()
    {
        $this->add('no-print-media-type');
        return $this;
    }

    /**
     * --proxy <proxy>                 Use a proxy
     * @param $proxy
     * @return Page
     */
    public function proxy($proxy)
    {
        $this->add('proxy', $proxy);
        return $this;
    }

    /**
     * --radiobutton-checked-svg <path> Use this SVG file when rendering checked radiobuttons
     * @param $path
     * @return Page
     */
    public function radiobuttonCheckedSvg($path)
    {
        $this->add('radiobutton-checked-svg', $path);
        return $this;
    }

    /**
     * --radiobutton-svg <path>        Use this SVG file when rendering unchecked radiobuttons
     * @param $path
     * @return Page
     */
    public function radiobuttonSvg($path)
    {
        $this->add('radiobutton-svg', $path);
        return $this;
    }

    /**
     * --resolve-relative-links        Resolve relative external links into absolute links (default)
     * @return Page
     */
    public function resolveRelativeLinks()
    {
        $this->add('resolve-relative-links');
        return $this;
    }

    /**
     * --run-script <js>               Run this additional javascript after the page is done loading (repeatable)
     * @param $js
     * @return Page
     */
    public function runScript($js)
    {
        $this->add('run-script', $js, true);
        return $this;
    }

    /**
     * --disable-smart-shrinking       Disable the intelligent shrinking strategy used by WebKit that makes the pixel/dpi ratio none constant
     * @return Page
     */
    public function disableSmartShrinking()
    {
        $this->add('disable-smart-shrinking');
        return $this;
    }

    /**
     * --enable-smart-shrinking        Enable the intelligent shrinking strategy used by WebKit that makes the pixel/dpi ratio none constant (default)
     * @return Page
     */
    public function enableSmartShrinking()
    {
        $this->add('enable-smart-shrinking');
        return $this;
    }

    /**
     * --stop-slow-scripts             Stop slow running javascripts (default)
     * @return Page
     */
    public function stopSlowScripts()
    {
        $this->add('stop-slow-scripts');
        return $this;
    }

    /**
     * --no-stop-slow-scripts          Do not Stop slow running javascripts
     * @return Page
     */
    public function noStopSlowScripts()
    {
        $this->add('no-stop-slow-scripts');
        return $this;
    }

    /**
     * --disable-toc-back-links        Do not link from section header to toc (default)
     * @return Page
     */
    public function disableTocBackLinks()
    {
        $this->add('disable-toc-backlinks');
        return $this;
    }

    /**
     * --enable-toc-back-links         Link from section header to toc
     * @return Page
     */
    public function enableTocBackLinks()
    {
        $this->add('enable-toc-backlinks');
        return $this;
    }

    /**
     * --user-style-sheet <url>        Specify a user style sheet, to load with every page
     * @param $url
     * @return Page
     */
    public function userStyleSheet($url)
    {
        $this->add('user-style-sheet', $url);
        return $this;
    }

    /**
     * --username <username>           HTTP Authentication username
     * @param $username
     * @return Page
     */
    public function username($username)
    {
        $this->add('username', $username);
        return $this;
    }

    /**
     * --viewport-size <>              Set viewport size if you have custom scrollbars or css attribute overflow to emulate window size
     * @param $size
     * @return Page
     */
    public function viewportSize($size)
    {
        $this->add('viewport-size', $size);
        return $this;
    }

    /**
     * --window-status <windowStatus>  Wait until window.status is equal to this string before rendering page
     * @param $windowStatus
     * @return Page
     */
    public function windowStatus($windowStatus)
    {
        $this->add('window-status', $windowStatus);
        return $this;
    }

    /**
     * --zoom <float>                  Use this zoom factor (default 1)
     * @param $float
     * @return Page
     */
    public function zoom($float)
    {
        $this->add('zoom', $float);
        return $this;
    }

    /**
     * --footer-center <text>          Centered footer text
     * @param string $text
     * @return Page
     */
    public function footerCenter($text)
    {
        $this->add('footer-center', $text);
        return $this;
    }

    /**
     * --footer-font-name <name>       Set footer font name (default Arial)
     * @param $name
     * @return Page
     */
    public function footerFontName($name)
    {
        $this->add('footer-font-name', $name);
        return $this;
    }

    /**
     * --footer-font-size <size>       Set footer font size (default 12)
     * @param $float
     * @return Page
     */
    public function footerFontSize($float)
    {
        $this->add('footer-font-size', $float);
        return $this;
    }

    /**
     * --footer-html <url>             Adds a html footer
     * @param $url
     * @return Page
     */
    public function footerHtml($url)
    {
        $this->add('footer-html', $url);
        return $this;
    }

    /**
     * --footer-left <text>            Left aligned footer text
     * @param $text
     * @return Page
     */
    public function footerLeft($text)
    {
        $this->add('footer-left', $text);
        return $this;
    }

    /**
     * --footer-line                   Display line above the footer
     * @return Page
     */
    public function footerLine()
    {
        $this->add('footer-line');
        return $this;
    }

    /**
     * --no-footer-line                Do not display line above the footer (default)
     * @return Page
     */
    public function noFooterLine()
    {
        $this->add('no-footer-line');
        return $this;
    }

    /**
     * --footer-right <text>           Right aligned footer text
     * @param $text
     * @return Page
     */
    public function footerRight($text)
    {
        $this->add('footer-right', $text);
        return $this;
    }

    /**
     * --footer-spacing <real>         Spacing between footer and content in mm (default 0)
     * @param $real
     * @return Page
     */
    public function footerSpacing($real)
    {
        $this->add('footer-spacing', $real);
        return $this;
    }

    /**
     * --header-center <text>          Centered header text
     * @param $text
     * @return Page
     */
    public function headerCenter($text)
    {
        $this->add('header-center', $text);
        return $this;
    }

    /**
     * --header-font-name <name>       Set header font name (default Arial)
     * @param $name
     * @return Page
     */
    public function headerFontName($name)
    {
        $this->add('header-font-name', $name);
        return $this;
    }

    /**
     * --header-font-size <size>       Set header font size (default 12)
     * @param $size
     * @return Page
     */
    public function headerFontSize($size)
    {
        $this->add('header-font-size', $size);
        return $this;
    }

    /**
     * --header-html <url>             Adds a html header
     * @param $url
     * @return Page
     */
    public function headerHtml($url)
    {
        $this->add('header-html', $url);
        return $this;
    }

    /**
     * --header-left <text>            Left aligned header text
     * @param $text
     * @return Page
     */
    public function headerLeft($text)
    {
        $this->add('header-left', $text);
        return $this;
    }

    /**
     * --header-line                   Display line below the header
     * @return Page
     */
    public function headerLine()
    {
        $this->add('header-line');
        return $this;
    }

    /**
     * --no-header-line                Do not display line below the header (default)
     * @return Page
     */
    public function noHeaderLine()
    {
        $this->add('no-header-line');
        return $this;
    }

    /**
     * --header-right <text>           Right aligned header text
     * @param $text
     * @return Page
     */
    public function headerRight($text)
    {
        $this->add('header-right', $text);
        return $this;
    }

    /**
     * --header-spacing <real>         Spacing between header and content in mm (default 0)
     * @param $real
     * @return Page
     */
    public function headerSpacing($real)
    {
        $this->add('header-spacing', $real);
        return $this;
    }

    /**
     * --replace <name> <value>        Replace [name] with value in header and footer (repeatable)
     * @param $name
     * @param $value
     * @return Page
     */
    public function replace($name, $value)
    {
        $this->add('replace', ['name' => $name, 'value' => $value], true);
        return $this;
    }
}
