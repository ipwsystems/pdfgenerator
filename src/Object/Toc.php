<?php

namespace IpwSystems\PdfGenerator\Object;

/**
 * Class Toc
 *
 * Since we havent used Table of Contents in our documents yet, this class has only been created as a placeholder for future reference.
 *
 * @package IpwSystems\PdfGenerator
 */
class Toc extends Page
{
    const TYPE = 'toc';
}
