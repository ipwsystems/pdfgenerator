<?php

namespace IpwSystems\PdfGenerator\Object;

/**
 * Class Options
 * @package IpwSystems\PdfGenerator
 */
class Options
{
    const TYPE = '';
    protected $url = '';
    private $options = [];

    public function __construct($url = '')
    {
        $this->url = $url;
    }

    /**
     * Parse all options into command line arguments
     *
     * @param string $option
     * @param mixed $value
     * @return array
     */
    protected static function parseValues($option, $value)
    {
        if ($value === true) {
            return ["--" . $option];
        }

        if (is_array($value)) {
            if (array_is_list($value)) {
                return array_map(
                    'static::parseValues',
                    array_fill(0, count($value), $option),
                    $value
                );
            }
        }

        return array_merge(["--" . $option], (array)$value);
    }

    /**
     *
     * @return array
     */
    public function options()
    {
        return array_filter(
            array_merge(
                [
                    static::TYPE,
                    $this->url ? '"' . $this->url . '"' : '',
                ],
                array_map('static::parseValues', array_keys($this->options), $this->options)
            )
        );
    }

    /**
     * Add an option to the page.
     *
     * @param string $option Name of the option.
     * @param mixed $value The value to add.
     * @param bool $asArray Whether the option is repeatable.
     */
    protected function add($option, $value = true, $asArray = false)
    {
        if ($asArray) {
            if (!isset($this->options[$option])) {
                $this->options[$option] = [];
            }
            $this->options[$option][] = $value;
            return;
        }
        $this->options[$option] = $value;
    }

}
