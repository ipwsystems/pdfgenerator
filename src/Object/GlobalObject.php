<?php

namespace IpwSystems\PdfGenerator\Object;

/**
 * Class GlobalObject
 * @package IpwSystems\PdfGenerator
 */
class GlobalObject extends Page
{
    const TYPE = '';

    /**
     * --collate                       Collate when printing multiple copies (default)
     * @return GlobalObject
     */
    public function collate()
    {
        $this->add('collate');
        return $this;
    }

    /**
     * --no-collate                    Do not collate when printing multiple copies
     * @return GlobalObject
     */
    public function noCollate()
    {
        $this->add('no-collate');
        return $this;
    }

    /**
     * --cookie-jar <path>             Read and write cookies from and to the supplied cookie jar file
     * @param $path
     * @return GlobalObject
     */
    public function cookieJar($path)
    {
        $this->add('cookie-jar', $path);
        return $this;
    }

    /**
     * --copies <number>               Number of copies to print into the pdf file (default 1)
     * @param $number
     * @return GlobalObject
     */
    public function copies($number)
    {
        $this->add('copies', $number);
        return $this;
    }

    /**
     * --dpi <dpi>                     Change the dpi explicitly (this has no effect on X11 based systems) (default 96)
     * @param $dpi
     * @return GlobalObject
     */
    public function dpi($dpi)
    {
        $this->add('dpi', $dpi);
        return $this;
    }

    /**
     * --grayscale                     PDF will be generated in grayscale
     * @return GlobalObject
     */
    public function grayscale()
    {
        $this->add('grayscale');
        return $this;
    }

    /**
     * --image-dpi <integer>           When embedding images scale them down to this dpi (default 600)
     * @param $integer
     * @return GlobalObject
     */
    public function imageDpi($integer)
    {
        $this->add('image-dpi', $integer);
        return $this;
    }

    /**
     * --image-quality <integer>       When jpeg compressing images use this quality (default 94)
     * @param $integer
     * @return GlobalObject
     */
    public function imageQuality($integer)
    {
        $this->add('image-quality', $integer);
        return $this;
    }

    /**
     * --lowquality                    Generates lower quality pdf/ps. Useful to shrink the result document space
     * @return GlobalObject
     */
    public function lowquality()
    {
        $this->add('lowquality');
        return $this;
    }

    /**
     * --margin-bottom <unitreal>      Set the page bottom margin
     * @param $unitreal
     * @return GlobalObject
     */
    public function marginBottom($unitreal)
    {
        $this->add('margin-bottom', $unitreal);
        return $this;
    }

    /**
     * --margin-left <unitreal>        Set the page left margin (default 10mm)
     * @param $unitreal
     * @return GlobalObject
     */
    public function marginLeft($unitreal)
    {
        $this->add('margin-left', $unitreal);
        return $this;
    }

    /**
     * --margin-right <unitreal>       Set the page right margin (default 10mm)
     * @param $unitreal
     * @return GlobalObject
     */
    public function marginRight($unitreal)
    {
        $this->add('margin-right', $unitreal);
        return $this;
    }

    /**
     * --margin-top <unitreal>         Set the page top margin
     * @param $unitreal
     * @return GlobalObject
     */
    public function marginTop($unitreal)
    {
        $this->add('margin-top', $unitreal);
        return $this;
    }

    /**
     * --orientation <orientation>     Set orientation to Landscape or Portrait (default Portrait)
     * @param $orientation
     * @return GlobalObject
     */
    public function orientation($orientation)
    {
        $this->add('orientation', $orientation);
        return $this;
    }

    /**
     * --page-height <unitreal>        Page height
     * @param $unitreal
     * @return GlobalObject
     */
    public function pageHeight($unitreal)
    {
        $this->add('page-height', $unitreal);
        return $this;
    }

    /**
     * --page-size <Size>              Set paper size to: A4, Letter, etc. (default A4)
     * @param $size
     * @return GlobalObject
     */
    public function pageSize($size)
    {
        $this->add('page-size', $size);
        return $this;
    }

    /**
     * --page-width <unitreal>         Page width
     * @param $unitreal
     * @return GlobalObject
     */
    public function pageWidth($unitreal)
    {
        $this->add('page-width', $unitreal);
        return $this;
    }

    /**
     * --no-pdf-compression            Do not use lossless compression on pdf objects
     * @return GlobalObject
     */
    public function noPdfCompression()
    {
        $this->add('no-pdf-compression');
        return $this;
    }

    /**
     * --title <text>                  The title of the generated pdf file (The title of the first document is used if not specified)
     * @param $text
     * @return GlobalObject
     */
    public function title($text)
    {
        $this->add('title', $text);
        return $this;
    }

    /**
     * --use-xserver                   Use the X server (some plugins and other stuff might not work without X11)
     * @return GlobalObject
     */
    public function useXserver()
    {
        $this->add('use-xserver');
        return $this;
    }

    /**
     * --dump-outline                   Dump the outline to a file
     * @param string $file
     * @return GlobalObject
     */
    public function dumpOutline($file)
    {
        $this->add('dump-outline', $file);
        return $this;
    }

    /**
     * --dump-default-toc-xsl           Dump the default TOC xsl style sheet to stdout
     * @return GlobalObject
     */
    public function dumpDefaultTocXsl()
    {
        $this->add('dump-default-toc-xsl');
        return $this;
    }

    /**
     * --outline                        Put an outline into the pdf (default)
     * @return GlobalObject
     */
    public function outline()
    {
        $this->add('outline');
        return $this;
    }

    /**
     * --no-outline                     Do not put an outline into the pdf
     * @return GlobalObject
     */
    public function noOutline()
    {
        $this->add('no-outline');
        return $this;
    }

    /**
     * --outline-depth                  Set the depth of the outline (default 4)
     * @param int $level 1 - 6
     * @return GlobalObject
     */
    public function outlineDepth($level)
    {
        $this->add('outline-depth', $level);
        return $this;
    }
}
